<?php

use Petcorp\Operation\PingOperation;
use Petcorp\Operation\ResultOperation;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;

include  __DIR__ . '/../vendor/autoload.php';

// текущая директория выше!
chdir(dirname(__DIR__));

$app = AppFactory::create();


$app->group('/', function (RouteCollectorProxy $group) {
    $group->get('ping', PingOperation::class);
    $group->post('result', ResultOperation::class);
});

//die('1231231');
//$errorMiddleware = $app->addErrorMiddleware(true, false, false);

$app->run();
