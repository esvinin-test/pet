<?php

namespace Petcorp\Operation;

use Slim\Psr7\Request;

class PingOperation extends BaseOperation
{
    public function handle(Request $request)
    {
        $response = new \stdClass();
        $response->result = 'Pong';
        return $response;
    }
}