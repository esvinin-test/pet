<?php

namespace Petcorp\Operation;

use Slim\Psr7\Request;
use Slim\Psr7\Response;

abstract class BaseOperation
{
    public function __invoke(Request $apiRequest, Response $apiResponse)
    {
        // Выполнение операции
        $response = $this->handle($apiRequest) ?: new \stdClass();

        $jsonResponse = json_encode($response);
        $apiResponse->getBody()->write($jsonResponse);

        return $apiResponse
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    abstract public function handle(Request $request);
}
