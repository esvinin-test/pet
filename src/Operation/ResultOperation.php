<?php

namespace Petcorp\Operation;

use Slim\Psr7\Request;

class ResultOperation extends BaseOperation
{
    public function handle(Request $request)
    {
        $requestData = json_decode($request->getBody()->getContents());
        
        $ratingCalculator = new \Petcorp\RatingCalculator();
        $ratingCalculator->calculate($requestData);
        
        return $requestData;
    }
}