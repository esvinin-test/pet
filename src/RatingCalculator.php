<?php

namespace Petcorp;

class RatingCalculator
{
    /**
     * Передаем по ссылке, чтобы при сортировке не создать копию массива
     * @param array $teamsScores
     */
    public function calculate(array &$teamsScores)
    {
        $this->sort($teamsScores);
        $this->calculateRank($teamsScores);
    }
    
    public function sort(array &$teamsScores)
    {
        usort($teamsScores, function($teamAScore, $teamBScore) {
            if ($teamAScore->scores < $teamBScore->scores) {
                return 1;
            }
            elseif ($teamAScore->scores == $teamBScore->scores) {
                return 0;
            }
            
            return -1;
        });
    }
    
    public function calculateRank(array $teamsScores)
    {
        $rank = 1;
        
        $lastRating = null;
        
        foreach ($teamsScores as $data) {
            if ($lastRating && $lastRating->scores == $data->scores) {
                $data->rank = $lastRating->rank;
            }
            else {
                $data->rank = $rank;
            }
            
            $lastRating = $data;
            
            $rank++;
        }
    }
}