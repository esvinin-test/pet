<?php

namespace Petcorp;

/**
 * Тест для расчета рейтинга
 * @group unit
 */
class RatingCalculatorTest extends \PHPUnit\Framework\TestCase
{
    private function makeTeam($name, $scores)
    {
        $teamData = new \stdClass();
        $teamData->team = $name;
        $teamData->scores = $scores;
        
        return $teamData;
    }
    
    private function makeTeamWithRank($name, $scores, $rank)
    {
        $teamData = new \stdClass();
        
        $teamData->team   = $name;
        $teamData->scores = $scores;
        $teamData->rank   = $rank;

        return $teamData;
    }

    /**
     * Тестируем метод сортировки
     */
    public function testSort_arrayWithTeams_arraySorted()
    {
        // Arrange
        $data = [
            $this->makeTeam('name1', 50),
            $this->makeTeam('name2', 80),
            $this->makeTeam('name3', 70),
        ];
        
        $expected = [
            $this->makeTeam('name2', 80),
            $this->makeTeam('name3', 70),
            $this->makeTeam('name1', 50),
        ];
        
        $calculator = new RatingCalculator;
        
        // Act
        $calculator->sort($data);
        
        // Assert
        $this->assertEquals($expected, $data);
    }
    
    /**
     * Тестируем метод проставления рейтинга
     */
    public function testCalculateRank_arrayWithTeams_rankSeted()
    {
        // Arrange
        $data = [
            $this->makeTeam('name2', 80),
            $this->makeTeam('name3', 70),
            $this->makeTeam('name4', 70),
            $this->makeTeam('name1', 50),
        ];
        
        $expected = [
            $this->makeTeamWithRank('name2', 80, 1),
            $this->makeTeamWithRank('name3', 70, 2),
            $this->makeTeamWithRank('name4', 70, 2),
            $this->makeTeamWithRank('name1', 50, 4),
        ];
        
        $calculator = new RatingCalculator;
        
        // Act
        $calculator->calculateRank($data);
        
        // Assert
        $this->assertEquals($expected, $data);
    }
    
    /**
     * Тестируем метод расчте ранга
     */
    public function testCalculateRank_arrayWithTeams_AllMethodCalled()
    {
        // Arrange
        $fakeCalculator = $this->getMockBuilder(RatingCalculator::class)
            ->setMethods(['sort', 'calculateRank'])
            ->getMock();

        $fakeCalculator->expects($this->exactly(1))->method('sort');
        $fakeCalculator->expects($this->exactly(1))->method('calculateRank');
                
        $data = [];
        
        // Act
        $fakeCalculator->calculate($data);
    }
}
